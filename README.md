
# Rangkuman Materi Git Eduwork

Rangkuman materi tentang dasar-dasar penggunaan Git dari materi Eduwork yang author pahami.


## Authors

- [@lazuandi](https://www.twitter.com/lazuandi)

peserta didik Eduwork untuk kelas MERN


## Pengertian

Git adalah _version control_ yang digunakan untuk mengembangkan perangkat lunak secara bersama-sama dengan pengembang lain ataupun sendiri agar mudah melakukan _tracking_ dalam perubahan kode



## Commands

1. Git init

- Digunakan untuk menginisialisasi repo
- Wajib dilakukan karena membuat git tanpa init tidak mungkin dilakukan

ketik ini di command line pada perangkat yang digunakan

``` git init ```

2. Git config
- Konfigurasi email dan username
- Biasanya disamakan dengan akun gitlab / github
- Config global untuk konfigurasi ke server
- Config local untuk konfigurasi local
- Untuk melihat semua konfigurasi yang ada di git bisa menggunakan perintah  git config --list 

``` git config list ```

3. Git status

- digunakkan untuk mengetahui status file

``` git status ```

4. Git add

- berfungsi untuk memasukkan file ke staging

``` git add . ```

5. Git Log

- git log dilakukan untuk melihat riwayat commit
- tekan tombol Q untuk keluar dari git log

``` git log ```

6. Git Commit
- untuk mencatat perubahan yang dilakukan menambahkan -melihat

``` git commit -m "pesan yang ingin ditulis" ```

7. Git Push & Git Pull

- git push dipergunakan untuk mengirimkan commit ke server

``` git push origin main ```

- git pull kebalikan dari push adalah untuk mengambil commit dari server ke lokal apabila ada perubahan di branch lain

``` git pull origin [nama branch] ```

8. Git Clone & Git Remote

- Git clone digunakan untuk melakukan copy repo dari link repo yang kita tuju

``` git clone [link yang ingin direpo] ```

- Git remote digunakan untuk berinteraksi antara client dengan server

``` git remove -v ```

9. Git Branch

Adalah copy/cabang  repo 

- ```Git branch```: melihat semua branch (local)
- ```Git branch --all```: melihat semua branch (public)
- ```Git branch nama_branch```: membuat branch baru
- ```Git checkout nama_branch```: untuk berpindah branch
- ```Git branch -d nama_branch```: untuk menghapus branch
